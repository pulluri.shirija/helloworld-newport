/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "helloworld-newport.h"

#include <mildenhall/mildenhall.h>
#include <newport/newport.h>

/* Push to a personal repo/branch and customize the values below to test changes to the images */
#define CGIT_REPO "https://git.apertis.org/cgit/sample-applications/helloworld-newport.git/plain"
#define CGIT_BRANCH "h=master"

static const gchar *image_files[] = {
  CGIT_REPO "/helloworld-newport/resources/images/A-500.png?" CGIT_BRANCH,
  CGIT_REPO "/helloworld-newport/resources/images/Apertis-500.png?" CGIT_BRANCH
};

struct _HlwNewport
{
  /*< private >*/
  GApplication parent;

  NewportService *newport;           /* owned */
  NewportDownload *newport_download; /* owned */
  ClutterActor *stage;               /* owned */
  gchar *download_path;              /* owned */

  ClutterActor *image; /* unowned */
  guint cnt;
};

G_DEFINE_TYPE (HlwNewport, hlw_newport, G_TYPE_APPLICATION);

static void newport_start_download_cb (GObject *source_object,
                                       GAsyncResult *res,
                                       gpointer user_data);

static gboolean
timeout_cb (gpointer user_data)
{
  HlwNewport *self = user_data;

  newport_service_call_start_download (self->newport,
                                       image_files[self->cnt++ % G_N_ELEMENTS (image_files)],
                                       self->download_path,
                                       NULL,
                                       newport_start_download_cb,
                                       self);

  return FALSE;
}

static gboolean
on_stage_destroy (ClutterActor *stage,
                  gpointer user_data)
{
  GApplication *app = user_data;

  g_debug ("The main stage is destroyed.");

  /* chain up */
  CLUTTER_ACTOR_GET_CLASS (stage)
      ->destroy (stage);

  /* The main window is detroyed (closed) so the application
   * needs to be released */
  g_application_release (app);

  return TRUE;
}

static void
newport_download_progress_cb (NewportDownload *object,
                              guint64 cur_size,
                              guint64 total_size,
                              guint64 download_speed,
                              guint64 elapsed_time,
                              guint64 remaining_time,
                              gpointer user_data)
{
  g_debug ("Downloading %" G_GINT64_FORMAT " / %" G_GINT64_FORMAT,
           cur_size,
           total_size);
}

static void
newport_download_state_cb (NewportDownload *newport_download,
                           GParamSpec *pspec,
                           gpointer user_data)
{
  g_autoptr (GdkPixbuf) pixbuf = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (ClutterContent) image_content = NULL;
  g_autoptr (GFile) file = NULL;
  HlwNewport *self = user_data;
  guint state = newport_download_get_state (newport_download);
  const gchar *downloaded_path;

  if (state != NEWPORT_DOWNLOAD_STATE_SUCCESS)
    return;

  downloaded_path = newport_download_get_path (self->newport_download);

  g_debug ("Download completed (path: %s)", downloaded_path);
  pixbuf = gdk_pixbuf_new_from_file (downloaded_path, NULL);

  image_content = clutter_image_new ();
  clutter_image_set_data (CLUTTER_IMAGE (image_content),
                          gdk_pixbuf_get_pixels (pixbuf),
                          gdk_pixbuf_get_has_alpha (pixbuf)
                              ? COGL_PIXEL_FORMAT_RGBA_8888
                              : COGL_PIXEL_FORMAT_RGB_888,
                          gdk_pixbuf_get_width (pixbuf),
                          gdk_pixbuf_get_height (pixbuf),
                          gdk_pixbuf_get_rowstride (pixbuf),
                          &error);

  clutter_actor_set_content (self->image, image_content);
  clutter_actor_set_size (self->image,
                          gdk_pixbuf_get_width (pixbuf),
                          gdk_pixbuf_get_height (pixbuf));

  file = g_file_new_for_path (downloaded_path);
  g_file_delete (file, NULL, &error);

  g_clear_object (&self->newport_download);

  g_timeout_add (500, timeout_cb, self);
}

static void
newport_download_ready_cb (GObject *source_object,
                           GAsyncResult *res,
                           gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  HlwNewport *self = user_data;

  g_clear_object (&self->newport_download);
  self->newport_download =
      newport_download_proxy_new_for_bus_finish (res,
                                                 &error);
  if (error)
    {
      g_warning ("Failed to get Newport Download proxy (reason: %s", error->message);
      return;
    }

  g_signal_connect (self->newport_download,
                    "download-progress",
                    G_CALLBACK (newport_download_progress_cb),
                    self);

  g_signal_connect (self->newport_download,
                    "notify::state",
                    G_CALLBACK (newport_download_state_cb),
                    self);
}

static void
newport_start_download_cb (GObject *source_object,
                           GAsyncResult *res,
                           gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  g_autofree gchar *download_obj_path = NULL;

  HlwNewport *self = user_data;

  newport_service_call_start_download_finish (self->newport,
                                              &download_obj_path,
                                              res,
                                              &error);
  if (error)
    {
      g_warning ("Failed to start downloading (reason: %s)", error->message);
      return;
    }

  newport_download_proxy_new_for_bus (
      G_BUS_TYPE_SESSION,
      G_DBUS_PROXY_FLAGS_NONE,
      "org.apertis.Newport",
      download_obj_path,
      NULL,
      newport_download_ready_cb,
      self);
}

static void
newport_service_ready_cb (GObject *source_object,
                          GAsyncResult *res,
                          gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  HlwNewport *self = user_data;

  self->newport = newport_service_proxy_new_for_bus_finish (
      res,
      &error);

  if (error)
    {
      g_warning ("Failed to get Newport service proxy. (reason: %s)", error->message);
      return;
    }

  newport_service_call_start_download (self->newport,
                                       image_files[self->cnt++ % 2],
                                       self->download_path,
                                       NULL,
                                       newport_start_download_cb,
                                       self);
}

static void
startup (GApplication *app)
{
  HlwNewport *self = HLW_NEWPORT (app);

  g_application_hold (app);

  /* chain up */
  G_APPLICATION_CLASS (hlw_newport_parent_class)
      ->startup (app);

  /* build ui components */
  self->stage = mildenhall_stage_new (g_application_get_application_id (app));

  g_signal_connect (G_OBJECT (self->stage),
                    "destroy", G_CALLBACK (on_stage_destroy),
                    self);

  newport_service_proxy_new_for_bus (
      G_BUS_TYPE_SESSION,
      G_DBUS_PROXY_FLAGS_NONE,
      "org.apertis.Newport",
      "/org/apertis/Newport/Service",
      NULL,
      newport_service_ready_cb,
      self);

  self->image = clutter_actor_new ();
  clutter_actor_add_child (self->stage, self->image);
}

static void
activate (GApplication *app)
{
  HlwNewport *self = HLW_NEWPORT (app);

  clutter_actor_show (self->stage);
}

static void
hlw_newport_dispose (GObject *object)
{
  HlwNewport *self = HLW_NEWPORT (object);

  g_clear_object (&self->stage);
  g_clear_object (&self->newport);
  g_clear_object (&self->newport_download);
  g_clear_pointer (&self->download_path, g_free);

  G_OBJECT_CLASS (hlw_newport_parent_class)
      ->dispose (object);
}

static void
hlw_newport_class_init (HlwNewportClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->dispose = hlw_newport_dispose;

  app_class->startup = startup;
  app_class->activate = activate;
}

static void
hlw_newport_init (HlwNewport *self)
{
  self->download_path = g_build_filename (
      g_get_user_special_dir (G_USER_DIRECTORY_DOWNLOAD),
      "newport-download",
      NULL);
}
